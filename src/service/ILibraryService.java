package src.service;

import src.models.Book;
import src.models.Library;

public interface ILibraryService {

    void addBook(Library library, Book book);

    void deleteBook(Library library, Book book);

    Book[] findBook(Library library, String requestWord);

}
