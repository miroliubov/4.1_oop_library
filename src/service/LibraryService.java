package src.service;

import src.models.Book;
import src.models.Library;

import java.util.ArrayList;
import java.util.List;

public class LibraryService implements ILibraryService {

    @Override
    public void addBook(Library library, Book book) {
        for (int i = 0; i < library.getBooksStorage().length; i++) {
            if (library.getBooksStorage()[i] == null) {
                library.getBooksStorage()[i] = book;
                return;
            }
        }
    }

    @Override
    public void deleteBook(Library library, Book book) {
        for (int i = 0; i < library.getBooksStorage().length; i++) {
            if (library.getBooksStorage()[i] == null){
                continue;
            }
            if (library.getBooksStorage()[i].equals(book)) {
                library.getBooksStorage()[i] = null;
            }
        }
    }

    @Override
    public Book[] findBook(Library library, String requestWord) {
        List<Book> bookList = new ArrayList<>();
        String requestWordLowerCase = requestWord.toLowerCase();
        for (int i = 0; i < library.getBooksStorage().length; i++) {
            if (library.getBooksStorage()[i] == null){
                continue;
            }
            if (library.getBooksStorage()[i].getBookAuthor().toLowerCase().equals(requestWordLowerCase) ||
                    library.getBooksStorage()[i].getBookTitle().equals(requestWordLowerCase)) {
                bookList.add(library.getBooksStorage()[i]);
            }
        }
        return bookList.toArray(new Book[0]);
    }
}
