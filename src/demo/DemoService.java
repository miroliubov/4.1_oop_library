package src.demo;

import src.models.Book;
import src.models.Library;
import src.service.ILibraryService;
import src.service.LibraryService;

import java.util.Arrays;

public class DemoService implements IDemoService {

    public void execute(){

        Library library = new Library();
        ILibraryService iLibraryService = new LibraryService();

        System.out.println("Добавление 10 книг");

        iLibraryService.addBook(library, new Book("William Shakespeare", "Romeo and Juliet"));
        iLibraryService.addBook(library, new Book("Yevgeny Zamyatin", "We"));
        iLibraryService.addBook(library, new Book("Aldous Huxley", "Brave New World"));
        iLibraryService.addBook(library, new Book("William Shakespeare", "Othello"));
        iLibraryService.addBook(library, new Book("Ray Bradbury", "Fahrenheit 451"));
        iLibraryService.addBook(library, new Book("Ayn Rand", "Atlas Shrugged"));
        iLibraryService.addBook(library, new Book("Haruki Murakami", "1Q84"));
        iLibraryService.addBook(library, new Book("Leo Tolstoy", "War and Peace"));
        iLibraryService.addBook(library, new Book("Leo Tolstoy", "War and Peace"));
        iLibraryService.addBook(library, new Book("George Orwell", "Animal Farm"));
        System.out.println(Arrays.toString(library.getBooksStorage()) + "\n");

        System.out.println("Добавление 11 книги");

        iLibraryService.addBook(library, new Book("Extra book", "Not added"));
        System.out.println(Arrays.toString(library.getBooksStorage()) + "\n");

        System.out.println("Удаление одной книги");

        iLibraryService.deleteBook(library, new Book("Ray Bradbury", "Fahrenheit 451"));
        System.out.println(Arrays.toString(library.getBooksStorage()) + "\n");

        System.out.println("Добавление книги в освободившееся место");

        iLibraryService.addBook(library, new Book("Harper Lee", "To Kill a Mockingbird"));
        System.out.println(Arrays.toString(library.getBooksStorage()) + "\n");

        System.out.println("Поиск книги");
        System.out.println(Arrays.toString(iLibraryService.findBook(library, "William Shakespeare")) + "\n");

        System.out.println("Поиск книги");
        System.out.println(Arrays.toString(iLibraryService.findBook(library, "War and Peace")));

    }
}
